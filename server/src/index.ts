import express, { Application, Request, Response, NextFunction } from 'express';

const app: Application = express();

app.get('/', (req: Request, res: Response, next: NextFunction) => {
    res.send({ hi: 'Hello word' })
})

const PORT = process.env.PORT || 2908;

app.listen(PORT, () => console.log(`Serve runing in http://localhost:${PORT}`));